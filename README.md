# ORM Test App

The project created to show and increase programming skills in the field of software development for the Android platform. 
Application supprot 2 languages: English and Polish.

### Project increase skills:
* MVP architecure
* Retrofit
* Butterknife
* RxJava
* Material Design

## Getting Started

These instructions will get you run copy of the project on your local machine for development purposes. 

### Installing

You can install this product directly by IDE Android Studio during development process or by Android Debug Nrodge(ADB).

Simple solution for instalation by ADB. After you copy of program .apk file to you device you should:
1. Connect the device to the developer desktop environment with a USB cable. 
2. Open a Windows Command Line or Terminal and execute: 
```
adb devices
```
The output should show the attached device:
```
$ adb devices

List of devices attached
HT012P010203 device
```
3. To install the application, navigate to the folder containing the downloaded APK. In  example it is temp directory and install APK using adb. 
```
$ cd /c/temp

$ adb install thisIsTheAPKName.apk
```
4. If the device list is empty, or a given device is not listed, kill the ADB server by executing: 
```
adb kill-server
```
5. Execute adb devices again to restart the server, re-detect devices, and try again.


## Built With
* [Android Studio 2.3](https://developer.android.com/studio/index.html) - Integrated Development Environment
* [Gradle 2.3 plugin for android](https://gradle.org/docs#getting-started) - Build and Dependency Management
* [Realm for Android](https://realm.io/news/realm-for-android/) Mobile Database

## Additional Library
* [Android Supprot v7:25.2.0](https://developer.android.com/topic/libraries/support-library/packages.html) - Android support library
* [Android Supprot v4:25.2.0](https://developer.android.com/topic/libraries/support-library/packages.html) - Android support library
* [Android Supprot Design v25.2.0](https://developer.android.com/topic/libraries/support-library/packages.html) - Material Design components for Android
* [Square Picasso v2.5.2](http://square.github.io/picasso/) - Image library for Android
* [Square Retrofit v2.1.0](http://square.github.io/picasso/) - Http client for Android
* [RxJava and RxAndroid](https://github.com/ReactiveX/RxAndroid) - Reactive Extensions for Android
* [ButterKnife](http://jakewharton.github.io/butterknife/) - Field and method binding for Android views
* [JUnit](http://junit.org/junit4/) - Unit test framework
* [CircleImageView](https://github.com/hdodenhof/CircleImageView) - specyfic ImageView for Android

# User manual
First view presents list of person. When you first start app try to get data from AWS service, and store it in local Realm Database.

### Applicztion main view contains:
* Person List - list of persons.
* Search field - which allows to filter person. Application checked the names of people.
* Person favorite indicator - it shows whether a person is in user favorites. It is interactive button by which user can add or remove person from favorites. If person is inactive, favorite indicator is replaced by inactive indicator.
* Navigation - it allow to change view or apply filters. Navigation is describe in next paragraph.
![img](https://bytebucket.org/moominek/ormtestapp/raw/8a28d503e95a968587846a6f37e967a26cbfdd0d/docs/images/main.png?token=174d956eb1b08366d3cb3f934a53e2e120927c0c)

### Application main view with navigation:
* Person filters - allow to apply specific filters for person list. Person filters works both in the map and list view.
* Views - allow to change view of person presentation. Application can show list or map view.
* Data update - allows user to force update person data from network serwice.
![img](https://bytebucket.org/moominek/ormtestapp/raw/8a28d503e95a968587846a6f37e967a26cbfdd0d/docs/images/navigation.png?token=5388350bdf91357a3d8c5c7f76b2d9ae308346bb)

### Aplication person details view:
* Navigation Button - allows the user to back to the main view.
* Personal data - shows person data. Phone and mail field allow contact with the person directly from application. Address field allow show position in google map application.
* Favorite indicator - it shows whether a person is in user favorites. It is interactive button by which user can add or remove person from favorites. If person is inactive, favorite indicator is replaced by inactive indicator.
![img](https://bytebucket.org/moominek/ormtestapp/raw/8a28d503e95a968587846a6f37e967a26cbfdd0d/docs/images/details.png?token=3fea05a2041d0587c6986f09871e68438a7549cb)

## Author
**Robert Grabiński**


