package com.driftingpixel.ormtestapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class Utility {

    /**
     * Load image to imageview by Picasso
     * @param context Android context
     * @param imageView image view
     * @param url url of image
     * @param imagePlaceHolder image placeholder, which is use if load error
     */
    public static void loadImageToImageView(Context context, ImageView imageView, String url, int imagePlaceHolder){
        Picasso.with(context)
                .load(url)
                .placeholder(imagePlaceHolder)
                .error(imagePlaceHolder)
                .into(imageView);
    }

    /**
     * Wraper for showSnackBar full function
     * @param context Android context
     * @param stringResource Android string resources
     */
    public static void showSnackBar(Context context, @StringRes int stringResource) {
        if(context == null)
            return;
        showSnackBar(context,context.getResources().getString(stringResource));
    }

    /**
     * Wraper for showSnackBar full function
     * @param context Android context
     * @param stringResource message content
     */
    public static void showSnackBar(Context context, String stringResource) {
        if(context == null)
            return;
        ViewGroup rootView = (ViewGroup) ((ViewGroup) ((Activity)context).findViewById(android.R.id.content)).getChildAt(0);
        showSnackBar(context,rootView, stringResource);
    }

    /**
     * Show snack info
     * @param context Android context
     * @param view root view
     * @param message message content
     */
    public static void showSnackBar(Context context, @NonNull View view, @StringRes String message) {
        if(context == null)
            return;
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.colorAlternative));
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.black_overlay));

        snackbar.show();
    }

    /**
     * Method chack internet connection status and show Alert Dialog if needed
     * @param activity curent Activity
     * @return true if internet is available
     */
    public static boolean checkIsInternetConnection(Activity activity) {
        boolean isNetwork = isNetworkConnection(activity);
        if (!isNetwork) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(activity, R.style.DialogStyle);
            builder.setTitle(activity.getResources().getString(R.string.no_internet_dialog_title));
            builder.setMessage(activity.getResources().getString(R.string.no_internet_dialog_message));
            builder.setNegativeButton("OK", null);
            builder.show();
        }

        return isNetwork;
    }

    /**
     * Method chack internet connection status
     * @param activity activity
     * @return true if internet is available
     */
    public static boolean isNetworkConnection(Activity activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    /**
     * Convert text to bitmap
     * @param text tect content
     * @param textSize text size
     * @param textColor text color
     * @return text as bitmap
     */
    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

}
