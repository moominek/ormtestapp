package com.driftingpixel.ormtestapp.model;


import java.util.List;

/**
 * Created by driftingPixel on 06.03.2017.
 */

public class ApplicationData {

    private List<Person> personList;
    private Person currentPerson;



    public void selectPerson(Person person){
        this.currentPerson = person;
    }

    public boolean updatePerson(Person person) {
        for(int i=0; i<personList.size(); i++)
            if(personList.get(i).getId() == person.getId()) {
                personList.set(i,person);
                return true;
            }

        return false;
    }





    /****************************************************
     * Getters and Setters
     ****************************************************/

    public Person getCurrentPerson() {
        return currentPerson;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }
}
