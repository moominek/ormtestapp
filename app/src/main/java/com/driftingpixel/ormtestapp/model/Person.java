package com.driftingpixel.ormtestapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by driftingPixel on 07.03.2017.
 */

public class Person extends RealmObject{

    @PrimaryKey
    private String id;

    private String firstName;
    private String lastName;
    private String picture;
    private String email;
    private String phone;
    private String address;
    private String city;
    private String about;
    private String favoriteFruit;
    private String latitude;
    private String longitude;



    private int age;


    private boolean active;
    private boolean favorite;

    public Person(){

    }

    public Person(String id, String firstName, String lastName, String picture, String email, String phone, String address, String city, String about, String favoriteFruit, int age, String latitude, String longitude, boolean active) {
        this(id, firstName, lastName, picture, email, phone, address, city, about, favoriteFruit, age, latitude, longitude, active, false);
    }

    public Person(String id, String firstName, String lastName, String picture, String email, String phone, String address, String city, String about, String favoriteFruit, int age, String latitude, String longitude, boolean active, boolean favorite) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.city = city;
        this.about = about;
        this.favoriteFruit = favoriteFruit;
        this.age = age;
        this.active = active;
        this.favorite = favorite;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    /***********************************************************
     *  Getters and Setters
     ***********************************************************/


    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPicture() {
        return picture;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getAbout() {
        return about;
    }

    public String getFavoriteFruit() {
        return favoriteFruit;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getAge() {
        return age;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isFavorite() {
        return favorite;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setFavoriteFruit(String favoriteFruit) {
        this.favoriteFruit = favoriteFruit;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName(){
        return firstName + " " + lastName;
    }
}

