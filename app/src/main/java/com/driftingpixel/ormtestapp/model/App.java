package com.driftingpixel.ormtestapp.model;

import android.app.Application;
import android.content.SharedPreferences;

import com.driftingpixel.ormtestapp.dataProviders.DbProviderRealm;
import com.driftingpixel.ormtestapp.dataProviders.IDbProvider;

import java.util.List;

/**
 * Created by driftingPixel on 05.03.2017.
 */

public class App extends Application {

    private static final String PREF_HAS_DATA_OFFLINE = "has_data_offline";
    private static final String PREF_SELECTED_USER_ID = "selected_user_id";

    private static App instance;
    private SharedPreferences preferences;
    private ApplicationData appData;
    private IDbProvider dbProvider;


    public static App getInstance(){
       return instance;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;
        instance.initializeInstance();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        dbProvider.closeDb();
    }

    protected void initializeInstance(){
        preferences = this.getApplicationContext().getSharedPreferences("ormTesAppPrefs",MODE_PRIVATE);
        appData = new ApplicationData();
        dbProvider = new DbProviderRealm(getApplicationContext());
    }


    /*************************************************************
     *  Getters and Setters
     *************************************************************/

    /**
     * Method resturns true if is some data stored offline
     * @return
     */
    public boolean hasDataOffline(){
        return this.preferences.getBoolean(PREF_HAS_DATA_OFFLINE,false);
    }

    /**
     * After stre value offline, application should invoke this method.
     * @param hasData
     */
    public void setHasDataOffline(boolean hasData){
        SharedPreferences.Editor edit = this.preferences.edit();
        edit.putBoolean(PREF_HAS_DATA_OFFLINE,hasData);
        edit.commit();
    }

    public String getSelectedUserId(){
        return this.preferences.getString(PREF_SELECTED_USER_ID,"");
    }

    public void setSelectedUserId(String id){
        SharedPreferences.Editor edit = this.preferences.edit();
        edit.putString(PREF_SELECTED_USER_ID,id);
        edit.commit();
    }

    public void savePersonList(List<Person> personList){
        dbProvider.savePersonList(personList);
        appData.setPersonList(personList);
    }

    public List<Person> getPersonList(String query, boolean onlyActivePerson, boolean onlyFavoritesPerson){

        List<Person> currntPersonList = dbProvider.getPersonList(query,onlyActivePerson, onlyFavoritesPerson);
        appData.setPersonList(currntPersonList);

        return currntPersonList;
    }


    public Person getCurrentPerson(){
        Person person = appData.getCurrentPerson();

        if(person == null){
            person = dbProvider.getPersonById(getSelectedUserId());
            appData.selectPerson(person);
        }

        return person;
    }

    public void selectPerson(Person person){
        appData.selectPerson(person);
        setSelectedUserId(person.getId());
    }

    public boolean updatePerson(Person person){
        return appData.updatePerson(person) && dbProvider.updatePerson(person);
    }

}
