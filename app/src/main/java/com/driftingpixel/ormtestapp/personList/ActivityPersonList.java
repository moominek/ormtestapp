package com.driftingpixel.ormtestapp.personList;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.driftingpixel.ormtestapp.R;
import com.driftingpixel.ormtestapp.Utility;
import com.driftingpixel.ormtestapp.dataProviders.network.NetworkConnection;
import com.driftingpixel.ormtestapp.model.Person;
import com.driftingpixel.ormtestapp.personDetails.ActivityPersonDetails;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityPersonList extends AppCompatActivity implements ViewPersonList, FragmentListView.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback{


    @BindView(R.id.fragment_container) RelativeLayout fragmentContainer;
    @BindView(R.id.progress) ProgressBar progressBar;
    @BindView(R.id.tv_progress_info) TextView tvProgressInfo;
    @BindView(R.id.btn_try_again) Button btnTryAgain;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.dl_activity_person_list) DrawerLayout drawerLayout;
    @BindView(R.id.nv_person_list_navigation_view) NavigationView navigationView;

    PresenterPersonList presenter;
    ViewPersonList currentFragment;
    FragmentListView fragmentPersonList;
    FragmentMapView mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        ButterKnife.bind(this);

        this.setTitle(getResources().getString(R.string.person_list_title));

        showListView();

        initializeToolbar();
    }

    private void initializeToolbar(){
        setSupportActionBar(toolbar);
        createAndSetDrawerLayoutListener();
        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorAlternative));
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    public void showProgressBar(String infoMessage) {
        if (infoMessage == null || infoMessage.equals(""))
            tvProgressInfo.setText(getResources().getString(R.string.load_person_info));
        else
            tvProgressInfo.setText(infoMessage);

        progressBar.setVisibility(View.VISIBLE);
        tvProgressInfo.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        tvProgressInfo.setText("");
        progressBar.setVisibility(View.INVISIBLE);

        tvProgressInfo.setVisibility(View.INVISIBLE);
    }

    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT))
            drawerLayout.closeDrawer(Gravity.LEFT);
        else if(currentFragment == mapFragment)
            showListView();
        else
            super.onBackPressed();
    }

    /**
     * Implementation of ViewPersonList interface
     * @param appErrorMessage
     */
    @Override
    public void onFailure(String appErrorMessage) {

    }

    /**
     * Implementation of ViewPersonList interface
     * @param personList
     */
    @Override
    public void getityListSuccess(List<Person> personList) {
        currentFragment.getityListSuccess(personList);
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showPersonDetails() {
        this.startActivity(new Intent(this, ActivityPersonDetails.class));
    }

    /**
     * Implementation of ViewPersonList interface
     * @param person person to update
     */
    @Override
    public void changeFavoriteStatus(Person person) {
        currentFragment.changeFavoriteStatus(person);

        Utility.showSnackBar(this,String.format(getResources().getString(person.isFavorite()? R.string.add_to_favorites:R.string.remove_from_favorites),person.getName()));
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showTryAgainButton() {
        progressBar.setVisibility(View.INVISIBLE);
        tvProgressInfo.setVisibility(View.INVISIBLE);
        btnTryAgain.setVisibility(View.VISIBLE);
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void clearList() {
        currentFragment.clearList();
    }

    /**
     * On click interface for try again button
     */
    @OnClick(R.id.btn_try_again)
    public void onBtnTryAgainClick(){
        btnTryAgain.setVisibility(View.INVISIBLE);
        presenter.forceUpdateFromApi();
    }

    /**
     * Action person selected
     * @param person
     */
    @Override
    public void onPersonSelected(Person person) {
        presenter.selectPerson(person);
    }

    /**
     * Action user try to change efavorite status of person
     * @param person person to update
     */
    @Override
    public void onTryChangeFavoriteStatus(Person person) {
        presenter.changeFavoriteStatus(person);
    }

    /**
     * Action search person
     * @param query serach query
     */
    @Override
    public void searchPerson(String query) {
        presenter.getPersonList(query);
    }

    private void createAndSetDrawerLayoutListener() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager imm = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    /**
     * Action navigation
     * @param item selected item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(Gravity.LEFT);

        switch (item.getItemId()) {
            case R.id.it_map_view:
                showMapView();
                break;
            case R.id.it_list_view:
                showListView();
                presenter.getPersonList("");
                break;
            case R.id.it_filter_active:
                presenter.showFilterResult(PresenterPersonList.FILTER_ACIVE_PERSON);
                break;
            case R.id.it_filter_favorites:
                presenter.showFilterResult(PresenterPersonList.FILTER_FAVORITE_PERSON);
                break;
            case R.id.it_filter_clear:
                presenter.showFilterResult(PresenterPersonList.FILTER_CLEAR);
                break;
            case R.id.it_update_from_api:
                presenter.forceUpdateFromApi();
                break;
        }

        return false;

    }

    private void showListView() {
        toolbar.setTitle(getString(R.string.person_list_title));
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentPersonList == null)
            fragmentPersonList = FragmentListView.newInstance();

        currentFragment = fragmentPersonList;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragmentPersonList);
        transaction.commit();
    }

    private void showMapView() {

        if(!Utility.checkIsInternetConnection(this))
            return;

        toolbar.setTitle(getString(R.string.map_view_title));

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (mapFragment == null)
            mapFragment = FragmentMapView.newInstance();

        currentFragment = mapFragment;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, mapFragment);
        transaction.commit();

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapFragment.setGoogleMap(googleMap);
        presenter.getPersonList("");
    }
}
