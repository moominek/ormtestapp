package com.driftingpixel.ormtestapp.personList;

import android.app.Activity;
import android.widget.Toast;

import com.driftingpixel.ormtestapp.R;
import com.driftingpixel.ormtestapp.Utility;
import com.driftingpixel.ormtestapp.dataProviders.network.AWSService;
import com.driftingpixel.ormtestapp.dataProviders.network.NetworkError;
import com.driftingpixel.ormtestapp.model.App;
import com.driftingpixel.ormtestapp.model.Person;

import java.util.List;

import rx.Subscription;
import rx.internal.util.UtilityFunctions;
import rx.subscriptions.CompositeSubscription;


public class PresenterPersonList {

    public static final String FILTER_ACIVE_PERSON = "Filter active person";
    public static final String FILTER_FAVORITE_PERSON = "Filter favorite person";
    public static final String FILTER_CLEAR = "Filter Clear";

    private final AWSService service;
    private final ViewPersonList view;
    private CompositeSubscription subscriptions;
    private List<Person> currentPersonList;

    public PresenterPersonList(AWSService service, ViewPersonList view){
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();

        getPersonList();
    }

    /**
     * Try to get full person list
     */
    public void getPersonList() {

        if(App.getInstance().hasDataOffline()){
            view.showProgressBar(null);
            getPersonListFromLocalDB("", false, false);
        }else{
            getPersonListFromNetworkAPI();
        }

    }

    /**
     * Try to get person list from local database
     * @param query search quesry
     * @param onlyActivePerson filter only active person
     * @param onlyFavoritesPerson filter only favorite person
     */
    public void getPersonList(String query, boolean onlyActivePerson, boolean onlyFavoritesPerson){
        getPersonListFromLocalDB(query, onlyActivePerson, onlyFavoritesPerson);
    }

    /**
     * Wraper fo getPersonList without filters
     * @param query
     */
    public void getPersonList(String query) {
        getPersonListFromLocalDB(query, false, false);
    }

    /**
     * Action Get person list from network api
     */
    private void getPersonListFromNetworkAPI(){
        view.clearList();

        if(!Utility.checkIsInternetConnection((Activity)view)) {

            view.showTryAgainButton();
            return;
        }

        view.showProgressBar(null);

        Subscription subscription = service.getPersonList(new AWSService.GetPersonListCallback() {
            @Override
            public void onSuccess(List<Person> personList) {

                App.getInstance().savePersonList(personList);
                view.hideProgressBar();
                view.getityListSuccess(App.getInstance().getPersonList("",false,false));
                Utility.showSnackBar(App.getInstance().getApplicationContext(), R.string.updated_from_api);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.hideProgressBar();
                view.onFailure(networkError.getAppErrorMessage());
            }

        });

        subscriptions.add(subscription);
    }

    /**
     * Get person list from local api
     * @param query search query
     * @param onlyActivePerson filter: only active persons
     * @param onlyFavoritesPerson filter: only favortie person
     */
    private void getPersonListFromLocalDB(String query,boolean onlyActivePerson, boolean onlyFavoritesPerson){
        view.hideProgressBar();
        view.getityListSuccess(App.getInstance().getPersonList(query, onlyActivePerson, onlyFavoritesPerson));
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    /**
     * Action select person
     * @param person
     */
    public void selectPerson(Person person){
        App.getInstance().selectPerson(person);
        view.showPersonDetails();
    }

    /**
     * Action change favorite status for person
     * @param person - person to update
     */
    public void changeFavoriteStatus(Person person){
        person.setFavorite(!person.isFavorite());
        if(App.getInstance().updatePerson(person))
            view.changeFavoriteStatus(person);
    }

    /**
     * Action show results with filters
     * @param filterFlag
     */
    public void showFilterResult(String filterFlag) {
        switch(filterFlag) {
            case FILTER_CLEAR:
                getPersonList("");
                break;
            case FILTER_ACIVE_PERSON:
                getPersonList("", true, false);
                break;
            case FILTER_FAVORITE_PERSON:
                getPersonList("", false, true);
        }
    }

    /**
     * Action force update data from api
     */
    public void forceUpdateFromApi(){
        getPersonListFromNetworkAPI();
    }

    /**
     * Get curent presenter person list
     * @return person list
     */
    public List<Person> getCurrentPersonList(){
        return currentPersonList;
    }
}
