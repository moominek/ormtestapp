package com.driftingpixel.ormtestapp.personList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.driftingpixel.ormtestapp.R;
import com.driftingpixel.ormtestapp.model.Person;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentListView extends Fragment implements ViewPersonList {

    @BindView(R.id.rv_person_list) RecyclerView rvPersonList;
    @BindView(R.id.tv_search_message) TextView tvSearchMessage;

    private SearchView mSearchView;
    private AdapterPersonList currentAdapter;
    private FragmentListView.OnFragmentInteractionListener interactionListener;


    public FragmentListView() {
        // Required empty public constructor
    }

    public static FragmentListView newInstance() {
        FragmentListView fragment = new FragmentListView();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person_list, container, false);
        ButterKnife.bind(this,view);

        setHasOptionsMenu(true);
        rvPersonList.setLayoutManager(new LinearLayoutManager(getContext()));
        if(currentAdapter != null) {
            currentAdapter.refreshContext(getContext());
            rvPersonList.setAdapter(currentAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            interactionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }


    /**
     * Interface to interaction with activity
     */
    public interface OnFragmentInteractionListener {
        void onPersonSelected(Person person);
        void onTryChangeFavoriteStatus(Person person);
        void searchPerson(String query);
    }


    //region implementation methods from ViewPersonList interface

    /**
     * Implementation of ViewPersonList interface
     * @param infoMessage
     */
    @Override
    public void showProgressBar(String infoMessage) {

    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void hideProgressBar() {

    }

    /**
     * Implementation of ViewPersonList interface
     * @param appErrorMessage
     */
    @Override
    public void onFailure(String appErrorMessage) {

    }

    /**
     * Implementation of ViewPersonList interface
     * @param personList person list to show
     */
    @Override
    public void getityListSuccess(List<Person> personList) {
        currentAdapter = new AdapterPersonList(getContext(), personList,
                new AdapterPersonList.OnItemInteractionListener(){
                    @Override
                    public void onItemClick(Person person) {
                        interactionListener.onPersonSelected(person);
                    }

                    @Override
                    public void onFavoriteClick(Person person) {
                        interactionListener.onTryChangeFavoriteStatus(person);
                    }
                });

        rvPersonList.setAdapter(currentAdapter);
        currentAdapter.notifyDataSetChanged();

        if(personList.size() == 0) {
            tvSearchMessage.setVisibility(View.VISIBLE);
            tvSearchMessage.setText(getString(R.string.search_message_no_result));
        }
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showPersonDetails() {

    }

    /**
     * Implementation of ViewPersonList interface
     * @param person
     */
    @Override
    public void changeFavoriteStatus(Person person) {
        ((AdapterPersonList)rvPersonList.getAdapter()).updatePersonWithId(person.getId(),person);
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showTryAgainButton() {

    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void clearList() {
        if(currentAdapter == null)
            return;

        currentAdapter.clearList();
        currentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);

        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        mSearchView.setOnSearchClickListener(new View.OnClickListener () {

            @Override
            public void onClick(View view) {
                hidePersonListAndShowSearchMessage();
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {

            @Override
            public boolean onClose() {
                tvSearchMessage.setVisibility(View.INVISIBLE);

                interactionListener.searchPerson("");

                return false;
            }
        });



        if(mSearchView != null) {

            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return sendPersonQuery(query);
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    return sendPersonQuery(query);
                }
            });
        }

    }

    private boolean sendPersonQuery(String query){
        if(query.length()>=3) {
            tvSearchMessage.setVisibility(View.INVISIBLE);
            interactionListener.searchPerson(query);
            return true;
        }else{
            hidePersonListAndShowSearchMessage();
        }
        return false;
    }

    private void hidePersonListAndShowSearchMessage(){
        tvSearchMessage.setVisibility(View.VISIBLE);
        tvSearchMessage.setText(getString(R.string.search_message_enter_3_ch));
        RecyclerView.Adapter adapter = rvPersonList.getAdapter();
        ((AdapterPersonList)adapter).clearList();
        adapter.notifyDataSetChanged();
    }

    //endregion
}
