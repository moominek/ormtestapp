package com.driftingpixel.ormtestapp.personList;


import com.driftingpixel.ormtestapp.model.Person;

import java.util.List;


public interface ViewPersonList {

    /**
     * Action show progress bar
     * @param infoMessage
     */
    void showProgressBar(String infoMessage);

    /**
     * Action hide progress bar
     */
    void hideProgressBar();

    /**
     * React to failure program try to get person list data
     * @param appErrorMessage
     */
    void onFailure(String appErrorMessage);

    /**
     * React to success when program try to get person list data
     * @param personList
     */
    void getityListSuccess(List<Person> personList);

    /**
     * Action show person data
     */
    void showPersonDetails();

    /**
     * Action change person favorite status
     * @param person
     */
    void changeFavoriteStatus(Person person);

    /**
     * React to no internet connection an is try again button needed
     */
    void showTryAgainButton();

    /**
     * Action clear showin list
     */
    void clearList();
}
