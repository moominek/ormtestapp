package com.driftingpixel.ormtestapp.personList;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.driftingpixel.ormtestapp.R;
import com.driftingpixel.ormtestapp.Utility;
import com.driftingpixel.ormtestapp.model.Person;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterPersonList extends RecyclerView.Adapter<AdapterPersonList.ViewHolder> {
    private final OnItemInteractionListener itemInteractionListener;
    private List<Person> personList;
    private Context context;


    public AdapterPersonList(Context context, List<Person> personList, OnItemInteractionListener itemInteractionListener) {
        this.personList = personList;
        this.itemInteractionListener = itemInteractionListener;
        this.context = context;
    }

    public void refreshContext(Context context){
        this.context = context;
    }


    @Override
    public AdapterPersonList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person_list, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AdapterPersonList.ViewHolder holder, int position) {
        Person person = personList.get(position);
        holder.addListeners(person, itemInteractionListener);
        holder.tvName.setText(person.getName());
        holder.tvAge.setText(""+person.getAge());
        holder.tvCity.setText(person.getCity());

        holder.ivFavorite.setImageDrawable(context.getResources().getDrawable(person.isFavorite() ? R.drawable.ic_fovorite_on : R.drawable.ic_favorite_off));

        if(!person.isActive()) {
            holder.ivFavorite.setVisibility(View.GONE);
            holder.tvInactive.setVisibility(View.VISIBLE);
        }
        Utility.loadImageToImageView(context,holder.ivPerson,person.getPicture(), R.drawable.placeholder_man);
    }


    @Override
    public int getItemCount() {
        return personList.size();
    }

    /**
     * Interface to interaction with activity
     */
    public interface OnItemInteractionListener {
        void onItemClick(Person person);
        void onFavoriteClick(Person person);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_person_city) TextView tvCity;
        @BindView(R.id.tv_person_age) TextView tvAge;
        @BindView(R.id.tv_person_name) TextView tvName;
        @BindView(R.id.civ_person_image) CircleImageView ivPerson;
        @BindView(R.id.iv_item_person_list_favorite) ImageView ivFavorite;
        @BindView(R.id.person_card_view) CardView cvPerson;
        @BindView(R.id.tv_item_person_inactive) TextView tvInactive;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }


        public void addListeners(final Person person, final OnItemInteractionListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(person);
                }
            });

            ivFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onFavoriteClick(person);
                }
            });
        }
    }

    public void updatePersonWithId(String id, Person person){
        for(int i=0; i<personList.size(); i++)
            if(personList.get(i).getId() == person.getId()) {
                personList.set(i,person);
                notifyItemChanged(i);
                return;
            }
    }

    public void clearList(){
        personList = new ArrayList<>();
    }

    public void setPersonList(List<Person> person){
        this.personList = personList;
    }

}
