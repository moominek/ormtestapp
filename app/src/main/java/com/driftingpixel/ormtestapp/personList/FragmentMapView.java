package com.driftingpixel.ormtestapp.personList;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.driftingpixel.ormtestapp.model.App;
import com.driftingpixel.ormtestapp.model.Person;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;


public class FragmentMapView extends SupportMapFragment implements ViewPersonList {

    public static final String TAG = "!!!FragmentMapView!!!";


    GoogleMap googleMap;
    public static FragmentMapView instance;

    public static FragmentMapView newInstance(){
        if(instance != null)
            return instance;
        else
            return instance = new FragmentMapView();
    }

    public FragmentMapView(){
        super();
    }

    /**
     * Implementation of ViewPersonList interface
     * @param infoMessage message for progress bar
     */
    @Override
    public void showProgressBar(String infoMessage) {

    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void hideProgressBar() {

    }

    /**
     * Implementation of ViewPersonList interface
     * @param appErrorMessage
     */
    @Override
    public void onFailure(String appErrorMessage) {

    }

    /**
     * Set google map object for fragment
     * @param googleMap
     */
    public void setGoogleMap(GoogleMap googleMap){
        this.googleMap = googleMap;
    }

    /**
     * Implementation of ViewPersonList interface
     * @param personList
     */
    @Override
    public void getityListSuccess(List<Person> personList) {

        googleMap.clear();

        LatLng positionForCamera = new LatLng(0,0);
        Geocoder geocoder = new Geocoder(App.getInstance().getApplicationContext());
        for (Person person: personList) {
            try {
                List<Address> addresses = geocoder.getFromLocationName(person.getAddress(), 1);
                if (addresses.size() > 0) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(positionForCamera = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                            .title(person.getName())).showInfoWindow();

                }else{
                    googleMap.addMarker(new MarkerOptions()
                            .position(positionForCamera = new LatLng(Float.parseFloat(person.getLatitude()), Float.parseFloat(person.getLongitude())))
                            .title(person.getName())).showInfoWindow();
                }
            }catch(IOException exception){
                Log.e(TAG, "IOException durring add amrkers to map" + exception.getMessage());
            }

        }


        googleMap.animateCamera(CameraUpdateFactory.zoomTo(2.0f));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(positionForCamera));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showPersonDetails() {
        //Do something if needed
    }

    /**
     * Implementation of ViewPersonList interface
     * @param person person to show
     */
    @Override
    public void changeFavoriteStatus(Person person) {
        //Do something if needed
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void showTryAgainButton() {
        //Do something if needed
    }

    /**
     * Implementation of ViewPersonList interface
     */
    @Override
    public void clearList() {
        //Do something if needed
    }
}
