package com.driftingpixel.ormtestapp.dataProviders;

import com.driftingpixel.ormtestapp.model.Person;

import java.util.List;

/**
 * Created by driftingPixel on 07.03.2017.
 */

public interface IDbProvider {


    List<Person> getPersonList(String query, boolean onlyActivePerson, boolean onlyFavoritesPerson);
    Person getPersonById(String id);
    void clearDb();
    void savePersonList(List<Person> personList);
    void closeDb();
    boolean updatePerson(Person person);

}
