package com.driftingpixel.ormtestapp.dataProviders.network;


import com.driftingpixel.ormtestapp.dataProviders.dataModels.PersonAWS;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Interface fo network api
 */
public interface NetworkService {

    @GET("/live")
    Observable<List<PersonAWS>> getPersonList();
}
