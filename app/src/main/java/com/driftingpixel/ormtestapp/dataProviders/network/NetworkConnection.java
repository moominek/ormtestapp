package com.driftingpixel.ormtestapp.dataProviders.network;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by driftingPixel on 05.03.2017.
 */

public class NetworkConnection {


    public NetworkConnection() {
    }

    public Retrofit provideCall() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .removeHeader("Pragma")
                                .header("Cache-Control", "no-cache")
                                .header("x-api-key", "k8bjyzaDdM6cMypKHiscO1b77kGaucbw67Ykdoo6")
                                .build();

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();
                        // Customize or return the response
                        return response;
                    }
                })
                .build();


        return new Retrofit.Builder()
                .baseUrl("https://pfxvxnbd5m.execute-api.eu-west-1.amazonaws.com")
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }


    @SuppressWarnings("unused")
    public NetworkService providesNetworkService() {
        return provideCall().create(NetworkService.class);
    }

    @SuppressWarnings("unused")
    public AWSService providesService() {
        return new AWSService(providesNetworkService());
    }

}
