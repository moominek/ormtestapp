package com.driftingpixel.ormtestapp.dataProviders.dataModels;

import com.driftingpixel.ormtestapp.model.Person;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "_id",
        "index",
        "guid",
        "isActive",
        "balance",
        "picture",
        "age",
        "eyeColor",
        "name",
        "company",
        "email",
        "phone",
        "address",
        "about",
        "registered",
        "latitude",
        "longitude",
        "tags",
        "range",
        "friends",
        "greeting",
        "favoriteFruit"
})
public class PersonAWS {

    @JsonProperty("_id")
    private String id;
    @JsonProperty("index")
    private Integer index;
    @JsonProperty("guid")
    private String guid;
    @JsonProperty("isActive")
    private Boolean isActive;
    @JsonProperty("balance")
    private String balance;
    @JsonProperty("picture")
    private String picture;
    @JsonProperty("age")
    private Integer age;
    @JsonProperty("eyeColor")
    private String eyeColor;
    @JsonProperty("name")
    private PersonNameAWS name;
    @JsonProperty("company")
    private String company;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("address")
    private String address;
    @JsonProperty("about")
    private String about;
    @JsonProperty("registered")
    private String registered;
    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonProperty("range")
    private List<Integer> range = null;
    @JsonProperty("friends")
    private List<PersonFriendAWS> friends = null;
    @JsonProperty("greeting")
    private String greeting;
    @JsonProperty("favoriteFruit")
    private String favoriteFruit;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("index")
    public Integer getIndex() {
        return index;
    }

    @JsonProperty("index")
    public void setIndex(Integer index) {
        this.index = index;
    }

    @JsonProperty("guid")
    public String getGuid() {
        return guid;
    }

    @JsonProperty("guid")
    public void setGuid(String guid) {
        this.guid = guid;
    }

    @JsonProperty("isActive")
    public Boolean getIsActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @JsonProperty("balance")
    public String getBalance() {
        return balance;
    }

    @JsonProperty("balance")
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @JsonProperty("picture")
    public String getPicture() {
        return picture;
    }

    @JsonProperty("picture")
    public void setPicture(String picture) {
        this.picture = picture;
    }

    @JsonProperty("age")
    public Integer getAge() {
        return age;
    }

    @JsonProperty("age")
    public void setAge(Integer age) {
        this.age = age;
    }

    @JsonProperty("eyeColor")
    public String getEyeColor() {
        return eyeColor;
    }

    @JsonProperty("eyeColor")
    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    @JsonProperty("name")
    public PersonNameAWS getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(PersonNameAWS name) {
        this.name = name;
    }

    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("about")
    public String getAbout() {
        return about;
    }

    @JsonProperty("about")
    public void setAbout(String about) {
        this.about = about;
    }

    @JsonProperty("registered")
    public String getRegistered() {
        return registered;
    }

    @JsonProperty("registered")
    public void setRegistered(String registered) {
        this.registered = registered;
    }

    @JsonProperty("latitude")
    public String getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public String getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonProperty("range")
    public List<Integer> getRange() {
        return range;
    }

    @JsonProperty("range")
    public void setRange(List<Integer> range) {
        this.range = range;
    }

    @JsonProperty("friends")
    public List<PersonFriendAWS> getFriends() {
        return friends;
    }

    @JsonProperty("friends")
    public void setFriends(List<PersonFriendAWS> friends) {
        this.friends = friends;
    }

    @JsonProperty("greeting")
    public String getGreeting() {
        return greeting;
    }

    @JsonProperty("greeting")
    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @JsonProperty("favoriteFruit")
    public String getFavoriteFruit() {
        return favoriteFruit;
    }

    @JsonProperty("favoriteFruit")
    public void setFavoriteFruit(String favoriteFruit) {
        this.favoriteFruit = favoriteFruit;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getCity(){
        String[] tmpArray = address.split(",");
        return tmpArray[tmpArray.length-2];
    }

    public Person toPerson(){
        return new Person(id,name.getFirst(), name.getLast(),picture,email,phone,address,getCity(),about,favoriteFruit,age, latitude, longitude, isActive, false);
    }
}