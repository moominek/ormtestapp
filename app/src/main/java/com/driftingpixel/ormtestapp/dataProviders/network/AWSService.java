package com.driftingpixel.ormtestapp.dataProviders.network;


import com.driftingpixel.ormtestapp.dataProviders.dataModels.PersonAWS;
import com.driftingpixel.ormtestapp.model.Person;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * AWS service provider
 */
public class AWSService {

    private final NetworkService networkService;

    public AWSService(NetworkService networkService) {
            this.networkService = networkService;
        }

    /**
     * Method get data from network service
     * @param callback - instance of Callback interface
     * @return ReX Subscription
     */
    public Subscription getPersonList(final GetPersonListCallback callback) {

        return networkService.getPersonList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends List<PersonAWS>>>() {
                    @Override
                    public Observable<? extends List<PersonAWS>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<List<PersonAWS>>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }
                    @Override
                    public void onNext(List<PersonAWS> personList) {
                        callback.onSuccess(projectToPersonList(personList));
                    }
                });
    }

    /**
     * Method project PersonAWS list o Person list.
     * @param personList Person list in AWS format
     * @return Person list in application model format
     */
    public List<Person> projectToPersonList(List<PersonAWS> personList){
        List<Person> result = new ArrayList<Person>();
        for (PersonAWS personAws: personList) {
            result.add(personAws.toPerson());
        }
        return result;
    }

    /**
     * Callback interface fo getPerson from network API list functionality
     */
    public interface GetPersonListCallback{
        void onSuccess(List<Person> personList);
        void onError(NetworkError networkError);
    }
}
