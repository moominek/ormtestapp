package com.driftingpixel.ormtestapp.dataProviders;

import android.content.Context;
import android.util.Log;

import com.driftingpixel.ormtestapp.model.App;
import com.driftingpixel.ormtestapp.model.Person;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * DB provider fo REALM
 */
public class DbProviderRealm implements IDbProvider{

    public static final String TAG = "!!!DbProvider REALM!!!";

    Realm realm;

    public DbProviderRealm(Context context){
        Realm.init(context);
    }

    /**
     * Get person list from Realm database
     * @param query - search query
     * @param onlyActivePerson - filter for only active person
     * @param onlyFavoritesPerson - filter for only favorite person
     * @return List of person
     */
    @Override
    public List<Person> getPersonList(String query, boolean onlyActivePerson, boolean onlyFavoritesPerson) {

        Log.d(TAG, "Try to get full person list");

        List<Person> result = new ArrayList<Person>();

        try {
            realm = Realm.getDefaultInstance();

            realm.beginTransaction();
            RealmQuery<Person> baseQuery = query.isEmpty()
                    ? realm.where(Person.class)
                    : realm.where(Person.class).contains("firstName",query, Case.INSENSITIVE).or().contains("lastName",query,Case.INSENSITIVE);

            if(onlyActivePerson)
                baseQuery = baseQuery.equalTo("active", true);

            if(onlyFavoritesPerson)
                baseQuery = baseQuery.equalTo("favorite", true);

            RealmResults results = baseQuery.findAll();

            result.addAll(realm.copyFromRealm(results));
            realm.commitTransaction();

        }catch(Exception exception){
            Log.e(TAG, "Exception during getting full Person list:" + exception.getMessage());
        }

        return result;
    }

    /**
     * Get person by id from Realm db
     * @param id person id
     * @return Person object with current id.
     */
    @Override
    public Person getPersonById(String id) {
        Log.d(TAG, "Try to get person by id: " + id);

        Person result;
        try{
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            result = realm.where(Person.class).equalTo("id",id).findFirst();
            realm.commitTransaction();
        }catch(Exception exception){
            result = null;
            Log.e(TAG, "Exception during clearing db:" + exception.getMessage());
        }

        return result;
    }

    /**
     * Clear realm database
     */
    @Override
    public void clearDb() {
        Log.d(TAG, "Try to clean person list");

        try{
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
            App.getInstance().setHasDataOffline(false);
        }catch(Exception exception){
            Log.e(TAG, "Exception during clearing db:" + exception.getMessage());
        }
    }

    /**
     * Clear database and save person list like new database
     * @param personList
     */
    @Override
    public void savePersonList(final List<Person> personList) {

        Log.d(TAG, "Try to save new person list");

        clearDb();


        try {
            realm = Realm.getDefaultInstance();

            Log.d(TAG, "Try execute transaction");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (Person person : personList) {
                        realm.copyToRealmOrUpdate(person);
                    }

                    App.getInstance().setHasDataOffline(true);
                }
            });
        } catch(Exception exception){
            Log.e(TAG, "Exception during getting full Person list:" + exception.getMessage());
        }

    }

    /**
     * Update person object in database by primary key
     * @param person object to save
     * @return Is object updated
     */
    @Override
    public boolean updatePerson(final Person person){
        Log.d(TAG, "Try to update person: " + person.getName());


        try {
            realm = Realm.getDefaultInstance();

            Log.d(TAG, "Try execute update transaction");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(person);
                }
            });
            return true;
        } catch(Exception exception){
            Log.e(TAG, "Exception during Update Person ->" + exception.getMessage());
            return false;
        }
    }

    /**
     * Close realm database
     */
    @Override
    public void closeDb() {
        realm.close();
    }
}
