package com.driftingpixel.ormtestapp.personDetails;

import com.driftingpixel.ormtestapp.R;

import com.driftingpixel.ormtestapp.model.App;
import com.driftingpixel.ormtestapp.model.Person;

/**
 * Created by driftingPixel on 06.03.2017.
 */

public class PresenterPersonDetails {

    public static final int USER_CLICK_PHONE = 999;
    public static final int USER_CLICK_MAIL = 998;
    public static final int USER_CLICK_ADDRESS = 997;

    ViewPersonDetails view;
    Person currentPerson;


    public PresenterPersonDetails(ViewPersonDetails view){
        this.view = view;

        currentPerson = App.getInstance().getCurrentPerson();
        view.initPersonView(currentPerson);
    }

    /**
     * Reaction to user actions
     * @param actionId
     */
    public void userClick(int actionId){
        switch(actionId) {
            case USER_CLICK_PHONE:
                view.callToPerson();
                break;
            case USER_CLICK_MAIL:
                view.mailToPerson();
                break;
            case USER_CLICK_ADDRESS:
                view.showPersonAddress();
                break;
        }
    }

    /**
     * Reaction on person favorite status change
     * @param person
     */
    public void changeFavoriteStatus(Person person){
        person.setFavorite(!person.isFavorite());
        if(App.getInstance().updatePerson(person))
            view.changeFavoriteStatus(person);
    }
}
