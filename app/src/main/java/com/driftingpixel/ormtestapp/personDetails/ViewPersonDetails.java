package com.driftingpixel.ormtestapp.personDetails;


import com.driftingpixel.ormtestapp.model.Person;

public interface ViewPersonDetails {
    /**
     * Initialize person view
     * @param person person to show
     */
    void initPersonView(Person person);

    /**
     * Action call to person
     */
    void callToPerson();

    /**
     * Action mail to person
     */
    void mailToPerson();

    /**
     * Action show persons address
     */
    void showPersonAddress();

    /**
     * Action change person favorite status
     * @param person person to change status
     */
    void changeFavoriteStatus(Person person);
}
