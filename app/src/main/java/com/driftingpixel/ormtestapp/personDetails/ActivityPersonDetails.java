package com.driftingpixel.ormtestapp.personDetails;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.driftingpixel.ormtestapp.R;
import com.driftingpixel.ormtestapp.Utility;

import com.driftingpixel.ormtestapp.dataProviders.network.NetworkConnection;
import com.driftingpixel.ormtestapp.model.Person;
import com.driftingpixel.ormtestapp.personList.PresenterPersonList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * View for Person Details
 */
public class ActivityPersonDetails extends AppCompatActivity implements ViewPersonDetails {

    @BindView(R.id.toolbar_person_details) Toolbar toolbarPersonDetails;
    @BindView(R.id.civ_person_details_image) CircleImageView civPersonImage;
    @BindView(R.id.collapsingToolbar_person_details) CollapsingToolbarLayout collapisnToolbar;
    @BindView(R.id.tv_person_details_phone) TextView tvPersonPhone;
    @BindView(R.id.tv_person_details_mail) TextView tvPersonMail;
    @BindView(R.id.tv_person_details_address) TextView tvPersonAddress;
    @BindView(R.id.tv_person_details_about) TextView tvPersonAbout;
    @BindView(R.id.fab_person_details_favorite) FloatingActionButton fabFavorite;

    public static final int CALLREQEST = 435;

    PresenterPersonDetails presenter;
    ActionBar toolbar;
    Person currentPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);

        ButterKnife.bind(this);
        customizeToolbar();

    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter = new PresenterPersonDetails(this);
    }

    private void customizeToolbar(){

        setSupportActionBar(toolbarPersonDetails);

        toolbar = getSupportActionBar();
        toolbar.setDisplayShowHomeEnabled(true);
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowCustomEnabled(true);
        toolbar.setDisplayShowTitleEnabled(true);
    }

    /**
     * Implementation of ViewPersonDetails Interface
     * @param person - person to show
     */
    @Override
    public void initPersonView(Person person) {

        currentPerson = person;

        toolbar.setTitle(currentPerson.getName());
        Utility.loadImageToImageView(this,civPersonImage,person.getPicture(),R.drawable.placeholder_man);

        tvPersonPhone.setText(currentPerson.getPhone());
        tvPersonMail.setText(currentPerson.getEmail());
        tvPersonAddress.setText(currentPerson.getAddress());
        tvPersonAbout.setText(currentPerson.getAbout());

        if(currentPerson.isActive()) {
            if (currentPerson.isFavorite())
                fabFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_fovorite_on));
        }else{
            fabFavorite.setImageBitmap(Utility.textAsBitmap(getResources().getString(R.string.person_inactive),getResources().getDimension(R.dimen.person_inactive_font_size),R.color.colorAlternativeDark));
            fabFavorite.setEnabled(false);
            fabFavorite.setScaleType(ImageView.ScaleType.CENTER);
        }
    }

    /**
     * Implementation of ViewPersonDetails Interface
     */
    @Override
    public void callToPerson() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + presenter.currentPerson.getPhone()));
        startActivity(callIntent);
    }

    /**
     * Implementation of ViewPersonDetails Interface
     */
    @Override
    public void mailToPerson() {
        Person currentPerson = presenter.currentPerson;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {currentPerson.getEmail()});
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject));
        String startText = getResources().getString(R.string.email_start_text);
        intent.putExtra(Intent.EXTRA_TEXT, String.format(startText, currentPerson.getName()));
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.email_app_chooser_title)));
    }

    /**
     * Implementation of ViewPersonDetails Interface
     */
    @Override
    public void showPersonAddress() {
        Person person = presenter.currentPerson;
        String uri = "geo:0,0?q=" + person.getAddress();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(Intent.createChooser(intent,String.format(getResources().getString(R.string.address_app_chooser_title), person.getName())));
    }

    /**
     * Implementation of ViewPersonDetails Interface
     * @param person person to update
     */
    @Override
    public void changeFavoriteStatus(Person person) {
        currentPerson = person;
        fabFavorite.setImageDrawable(getResources().getDrawable(person.isFavorite()?R.drawable.ic_fovorite_on:R.drawable.ic_favorite_off));
    }

    /**
     * On click interface do selected views
     * @param view
     */
    @OnClick({R.id.rl_person_details_phone, R.id.rl_person_details_mail, R.id.rl_person_details_address, R.id.fab_person_details_favorite})
    public void onBtnUserClick(View view)
    {
        switch(view.getId()) {
            case R.id.rl_person_details_phone:
                presenter.userClick(PresenterPersonDetails.USER_CLICK_PHONE);
                break;
            case R.id.rl_person_details_mail:
                presenter.userClick(PresenterPersonDetails.USER_CLICK_MAIL);
                break;
            case R.id.rl_person_details_address:
                presenter.userClick(PresenterPersonDetails.USER_CLICK_ADDRESS);
                break;
            case R.id.fab_person_details_favorite:
                presenter.changeFavoriteStatus(currentPerson);
        }
    }

}
