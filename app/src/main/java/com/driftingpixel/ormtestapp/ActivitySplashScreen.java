package com.driftingpixel.ormtestapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.driftingpixel.ormtestapp.personList.ActivityPersonList;

public class ActivitySplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, ActivityPersonList.class);
        startActivity(intent);
        finish();
    }
}
