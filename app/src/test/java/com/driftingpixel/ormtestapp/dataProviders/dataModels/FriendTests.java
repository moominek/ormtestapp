package com.driftingpixel.ormtestapp.dataProviders.dataModels;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import static org.junit.Assert.*;

public class FriendTests {

    @Test
    public void jsonToPerson() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        String friendJson =
                "      {\n" +
                        "        \"id\": 0,\n" +
                        "        \"name\": \"Claudine Russo\"\n" +
                        "      }";

        PersonFriend friend = mapper.readValue(friendJson, PersonFriend.class);

        assertEquals("Person Id should be 0", (int)friend.getId(), 0);
        assertEquals("Person name should be Claudine Russo", "Claudine Russo", friend.getName());
    }
}
