package com.driftingpixel.ormtestapp.dataProviders.dataModels;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import static org.junit.Assert.*;

public class PersonTests {

    @Test
    public void jsonToPerson() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        String personJson = "{\n" +
                "    \"_id\": \"5728799d89f5e06d069ccc7a\",\n" +
                "    \"index\": 0,\n" +
                "    \"guid\": \"deded478-6c03-4b46-a6e0-f59908ce6fa9\",\n" +
                "    \"isActive\": true,\n" +
                "    \"balance\": \"$3,085.61\",\n" +
                "    \"picture\": \"http://placehold.it/32x32\",\n" +
                "    \"age\": 28,\n" +
                "    \"eyeColor\": \"green\",\n" +
                "    \"name\": {\n" +
                "      \"first\": \"Barlow\",\n" +
                "      \"last\": \"Moore\"\n" +
                "    },\n" +
                "    \"company\": \"KONNECT\",\n" +
                "    \"email\": \"barlow.moore@konnect.tv\",\n" +
                "    \"phone\": \"+1 (837) 514-2845\",\n" +
                "    \"address\": \"12901 Cantrell, Little Rock, Arkansas 72227\",\n" +
                "    \"about\": \"Dolor culpa ullamco voluptate proident adipisicing est velit ex sint. Eu mollit in laborum consequat do cillum qui exercitation culpa ipsum do dolor consectetur quis. Eiusmod qui aliquip eiusmod velit esse nisi ut amet.\",\n" +
                "    \"registered\": \"Sunday, January 19, 2014 1:57 AM\",\n" +
                "    \"latitude\": \"-84.63438\",\n" +
                "    \"longitude\": \"17.604834\",\n" +
                "    \"tags\": [\n" +
                "      \"irure\",\n" +
                "      \"irure\",\n" +
                "      \"aliqua\",\n" +
                "      \"sint\",\n" +
                "      \"veniam\"\n" +
                "    ],\n" +
                "    \"range\": [\n" +
                "      0,\n" +
                "      1,\n" +
                "      2,\n" +
                "      3,\n" +
                "      4,\n" +
                "      5,\n" +
                "      6,\n" +
                "      7,\n" +
                "      8,\n" +
                "      9\n" +
                "    ],\n" +
                "    \"friends\": [\n" +
                "      {\n" +
                "        \"id\": 0,\n" +
                "        \"name\": \"Claudine Russo\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"Valdez Christian\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 2,\n" +
                "        \"name\": \"Jannie Horton\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"greeting\": \"Hello, Barlow! You have 8 unread messages.\",\n" +
                "    \"favoriteFruit\": \"apple\"\n" +
                "  }";

        Person person = mapper.readValue(personJson, Person.class);

        assertEquals("Person Id should be 5728799d89f5e06d069ccc7a", "5728799d89f5e06d069ccc7a", person.getId());
        assertEquals("Person should has 3 firends", 3, person.getFriends().size());
        assertTrue("Person should be active", person.getIsActive());
        assertEquals("person should be 28 years old",28, (int)person.getAge());

        //TODO more test for json parse and make expected value more connected with json.
    }
}