package com.driftingpixel.ormtestapp.dataProviders.dataModels;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class PersonNameTests {

    @Test
    public void jsonToPerson() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        String personNameJson = "{\n" +
                "      \"first\": \"Barlow\",\n" +
                "      \"last\": \"Moore\"\n" +
                "    }";

        PersonName personName = mapper.readValue(personNameJson, PersonName.class);

        assertEquals("Person first name should be Barlow", "Barlow", personName.getFirst());
        assertEquals("Person last name should be Moore", "Moore", personName.getLast());
    }

    @Test
    public void toStringTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        String personNameJson = "{\n" +
                "      \"first\": \"Barlow\",\n" +
                "      \"last\": \"Moore\"\n" +
                "    }";

        PersonName personName = mapper.readValue(personNameJson, PersonName.class);
        assertEquals("Person name should be: Barlow Moore", "Barlow Moore", personName.toString());
    }
}
